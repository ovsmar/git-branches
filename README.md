# Git branches

### 1. First, create a new repository on GitHub or a similar platform. You can name it whatever you want.

### 2.Clone the repository to your local machine using the following command in your terminal:

```bash
git clone <repository-url>
```


### 3. Navigate to the directory where you cloned the repository using the following command:

```bash
cd <repository-name>
```


### 4. Create a new branch called "front-end" using the following command:

```bash
git branch front-end
```


### 5. Switch to the "front-end" branch using the following command:

```bash
git checkout front-end
```


### 6. Create a simple front-end project in the directory. You can use any front-end framework you like, such as React, Vue, or Angular.

### 7. Add and commit your changes to the "front-end" branch using the following commands

```bash
git add .
git commit -m "Added front-end project"
```

### 8. Push the new branch to GitLab , This will create a new branch in the GitLab repository and push the changes you made locally to that branch.

```bash
git push -u origin <branch-name>
```

### 9. Switch back to the "master" or "main" branch using the following command:

```bash
git add .
git checkout master
```


### 10. Create a new branch called "back-end" using the following command:

```bash
git branch back-end
```


### 12. Switch to the "back-end" branch using the following command:

```bash
git checkout back-end
```


### 13. Create a simple back-end project in the directory. You can use any back-end framework you like, such as Node.js, Django, or Ruby on Rails.

### 14. Add and commit your changes to the "back-end" branch using the following commands:

```bash
git add .
git commit -m "Added back-end project"

```


### 15. Push the new branch to GitLab , This will create a new branch in the GitLab repository and push the changes you made locally to that branch.

```bash
git push -u origin <branch-name>
```


### 16. Switch back to the "master" or "main" branch using the following command:

```bash
git checkout master
```


### 17. Merge the "front-end" and "back-end" branches into the "master" branch using the following commands:

```bash
git merge front-end
git merge back-end

```


### 18. Push your changes to the remote repository using the following command:

```bash
git push
```


That's it! You now have a simple project with front-end and back-end branches. You can experiment with different ideas in each branch without affecting the main codebase, and merge them back into the master branch when you're ready.